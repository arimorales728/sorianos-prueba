<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class MusicalDisc extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'album',
        'year',
        'image',
        'artist_id',
        'music_genre_id',
    ];

    public function artist(): BelongsTo
    {
        return $this->belongsTo(Artist::class);
    }

    public function musicGenre(): BelongsTo
    {
        return $this->belongsTo(MusicGenre::class);
    }
}
