<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\Pivot;

class MusicGenreUser extends Pivot
{
    protected $table = 'music_genre_user';

    public function musicGenres(): BelongsToMany
    {
        return $this->belongsToMany(MusicGenre::class);
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }
}
