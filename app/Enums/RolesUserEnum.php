<?php

namespace App\Enums;

enum RolesUserEnum: string
{
    case ADMIN = 'admin';
    case CLIENT = 'client';
}
