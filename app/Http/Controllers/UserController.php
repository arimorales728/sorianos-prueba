<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileUpdateRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\UserResource;
use App\Models\MusicGenre;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules;
use Inertia\Inertia;
use Inertia\Response;
use Rules\Password;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $users = User::where('active', true)->with('profile', 'musicGenres')->get()->toArray();
        $genres  = MusicGenre::select(['id', 'name'])->get();
        $profiles  = Profile::select(['id', 'name'])->get();

        return Inertia::render('User', ['users' => $users, 'genres' => $genres, 'types' => $profiles]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|lowercase|email|max:255|unique:' . User::class,
            'phone' => 'required|min:10',
            'music_genres' => 'required|array|min:1',
            'avatar' => 'required|image',
            'profile_id' => 'required|integer',
            'password' => 'required|string|min:8',
        ]);

        $path = $this->saveImage($request);

        $user = User::create([
            'name' => $request->name,
            'avatar' => $path,
            'phone' => $request->phone,
            'email' => $request->email,
            'profile_id' => $request->profile_id,
            'password' => Hash::make($request->password),
            'remember_token' => Str::random(35),
        ]);

        $user->sendEmailVerificationNotification();

        $user->musicGenres()->sync($request->music_genres);
        event(new Registered($user));

        return redirect()->back()->with('message', 'Creado');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $user = User::where('id', $id)->with('musicGenres')->first();
        return Inertia::render('Users', [
            'authUser' => UserResource::make($user),
        ]);
    }

    public function showMe()
    {
        $user = User::where('id', Auth::user()->id)->with('musicGenres')->first();
        return Inertia::render('Profile/Edit', [
            'authUser' => $user->toArray()
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateUserRequest $request, string $id)
    {
        $user = User::where('id', $id)->first();

        $data = $request->validated();
        $data['avatar'] = $this->saveImage($request, $user->avatar);
        $user->fill($data);

        if ($user->isDirty('email')) {
            $user->email_verified_at = null;
        }

        $user->save();
        $user->musicGenres()->sync($request->music_genres);

        return redirect()->back()->with('message', 'Actualizado');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        User::where('id', $id)->update([
            'active' => false,
        ]);

        return redirect()->back()->with('message', 'Eliminado');
    }

    private function saveImage(Request $request, $default = null)
    {
        $path = null;
        if ($request->hasFile('avatar')) {
            $randomize = rand(111111, 999999);
            $extension = $request->file('avatar')->getClientOriginalExtension();
            $filename = 'user-' . $randomize . '.' . $extension;
            $path = $request->avatar->storeAs('avatars', $filename, 'public');
        }

        return $path ?? $default;
    }
}
