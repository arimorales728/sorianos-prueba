<?php

namespace App\Http\Controllers;

use App\Http\Resources\MusicDiscResource;
use App\Models\Artist;
use App\Models\MusicalDisc;
use App\Models\MusicGenre;
use Illuminate\Http\Request;

class MusicalDiscController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $artists = Artist::select(['id', 'name'])->get();
        $genres  = MusicGenre::select(['id', 'name'])->get();

        $discs = MusicalDisc::with(['artist', 'musicGenre'])
            ->when($request->search, fn ($query) => $query
                ->where('name', 'like', '%' . $request->search . '%')
                ->orWhere('album', 'like', '%' . $request->search . '%')
                ->orWhereHas('artist', fn ($query) => $query->where('name', 'like', '%' . $request->search . '%')))
            ->get();

        return inertia('Disc', [
            'artists' => $artists,
            'discs'   => $discs,
            'genres'  => $genres,
        ]);
    }

    public function store(Request $request)
    {
        $data = $this->validateData($request);
        $data['image'] = $this->saveImage($request);
        MusicalDisc::create($data);

        return redirect()->back()->with('message', 'Created');
    }

    /**
     * Display the specified resource.
     */
    public function show(MusicalDisc $disc)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, MusicalDisc $disc)
    {
        $data = $this->validateDataUpdate($request);
        $data['image'] = $this->saveImage($request, $disc->image);
        $disc->update($data);
        // dd($disc->toArray());

        return redirect()->back()->with('message', 'Updated');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(MusicalDisc $disc)
    {
        $disc->delete();
        return redirect()->back()->with('message', 'Deleted');
    }

    private function validateData(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'string'],
            'album' => ['required', 'string'],
            'year' => ['required', 'numeric', 'between:1,' . today()->year],
            'image' => ['required', 'image'],
            'artist_id' => ['required', 'numeric'],
            'music_genre_id' => ['required', 'numeric'],
        ]);

        return $data;
    }

    private function validateDataUpdate(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'string'],
            'album' => ['required', 'string'],
            'year' => ['required', 'numeric', 'between:1,' . today()->year],
            'image' => ['nullable', 'image'],
            'artist_id' => ['required', 'numeric'],
            'music_genre_id' => ['required', 'numeric'],
        ]);

        return $data;
    }
    private function saveImage(Request $request, $default = null)
    {
        $path = null;
        if ($request->hasFile('image')) {
            $randomize = rand(111111, 999999);
            $extension = $request->file('image')->getClientOriginalExtension();
            $filename = 'disc-cover-' . $randomize . '.' . $extension;
            $path = $request->image->storeAs('images', $filename, 'public');
        }

        return $path ?? $default;
    }
}
