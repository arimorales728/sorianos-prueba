<?php

namespace App\Http\Controllers;

use App\Models\MusicGenre;
use Illuminate\Http\Request;
use Inertia\Inertia;

class MusicGenreController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $list = MusicGenre::all()->pluck('name')->toArray();
        Inertia::setRootView('pages.auth');
        // return Inertia::render(['Register', 'Profile/Edit'], [
        //     'genres' => $list
        // ]);

        if ($request->get('page') === 'register') {
            return Inertia::render('Register', [
                'genres' => $list
            ]);
        } elseif ($request->get('page') === 'profile/edit') {
            return Inertia::render('Profile/Edit', [
                'genres' => $list
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
