<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ProfileUserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $profiles  = Profile::select(['id', 'name'])->withCount(['user' => function ($query) {
            $query->where('active', true);
        }])->get();

        return Inertia::render('Profiles', ['profiles' => $profiles]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'min:5'],
        ]);

        $user = Profile::create([
            'name' => $request->name,
        ]);
        return redirect()->back()->with('message', 'Creado');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'min:5'],
        ]);
        Profile::where('id', $id)->update([
            'name' => $request->name
        ]);

        return redirect()->back()->with('message', 'Actualizado');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        Profile::where('id', $id)->delete();
        return redirect()->back()->with('message', 'Eliminado');
    }
}
