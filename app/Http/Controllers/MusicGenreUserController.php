<?php

namespace App\Http\Controllers;

use App\Models\MusicGenreUser;
use Illuminate\Http\Request;

class MusicGenreUserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(MusicGenreUser $musicGenreUser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(MusicGenreUser $musicGenreUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, MusicGenreUser $musicGenreUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(MusicGenreUser $musicGenreUser)
    {
        //
    }
}
