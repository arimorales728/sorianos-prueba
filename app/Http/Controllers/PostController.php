<?php

namespace App\Http\Controllers;

use App\Http\Resources\PostResource;
use App\Models\Post;
use Illuminate\Http\Request;
use Inertia\Inertia;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $posts = Post::all()->toArray();
        return Inertia::render('Post', ['posts' => $posts]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        Post::create([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
        ]);
    }


    public function show(Post $post)
    {
        return PostResource::make($post);
    }

    public function update(Request $request, Post $post)
    {
        $post->update([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Post $post)
    {
        $post->delete();
    }
}
