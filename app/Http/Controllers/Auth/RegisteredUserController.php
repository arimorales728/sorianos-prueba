<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\MusicGenre;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules;
use Inertia\Inertia;
use Inertia\Response;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     */
    public function create(): Response
    {
        return Inertia::render('Auth/Register', [
            'genres' => MusicGenre::select(['id', 'name'])->get(),
        ]);
    }

    /**
     * Handle an incoming registration request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|lowercase|email|max:255|unique:' . User::class,
            'phone' => 'required|min:10',
            'interest' => 'required|array|min:1',
            'avatar' => 'required|image',
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        $path = $this->saveImage($request);

        $user = User::create([
            'name' => $request->name,
            'avatar' => $path,
            'phone' => $request->phone,
            'email' => $request->email,
            'password' => Hash::make($request->password),            
            'profile_id' => 2, /// Defecto Cliente
            'remember_token' => Str::random(35),
        ]);

        // $user->sendEmailVerificationNotification();

        $user->musicGenres()->sync($request->interest);
        event(new Registered($user));

        return redirect()->route('register-success');
    }

    private function saveImage(Request $request, $default = null)
    {
        $path = null;
        if ($request->hasFile('avatar')) {
            $randomize = rand(111111, 999999);
            $extension = $request->file('avatar')->getClientOriginalExtension();
            $filename = 'user-' . $randomize . '.' . $extension;
            $path = $request->avatar->storeAs('avatars', $filename, 'public');
        }

        return $path ?? $default;
    }
}
