<?php

use App\Http\Controllers\MusicalDiscController;
use App\Http\Controllers\MusicGenreController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ProfileUserController;
use App\Http\Controllers\UserController;
use App\Models\MusicGenre;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/register-success', function () {
    return Inertia::render('Auth/VerifyEmail');
})->name('register-success');

Route::middleware(['auth', 'verified'])->group(function () {
    Route::get('/', fn () =>  Inertia::render('Dashboard'))->name('dashboard');
    Route::get('/genres', [MusicGenreController::class, 'index']);
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::post('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    Route::apiResource('posts', PostController::class);
    Route::apiResource('discs', MusicalDiscController::class);
    Route::apiResource('user', UserController::class);
    Route::apiResource('profiles', ProfileUserController::class);
    Route::get('/post', [PostController::class, 'index'])->name('post');
    // Route::get('/user', [UserController::class, 'index'])->name('user');
    Route::get('/me-profile', [UserController::class, 'showMe'])->name('profile.showMe');
});

require __DIR__ . '/auth.php';
