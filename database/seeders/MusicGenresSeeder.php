<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MusicGenresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $json = file_get_contents(database_path('data/genres.json'));
        $genres = json_decode($json, true);
        foreach ($genres as $genre) {
            DB::table('music_genres')->insert([
                'name' => $genre['name'],
            ]);
        }
    }
}
