<?php

namespace Database\Seeders;

use App\Enums\RolesUserEnum;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::firstOrCreate(
            [
                'email' => 'admin@admin.com',
            ],
            [
                'avatar' => null,
                'name' => 'Admin',
                'email' => 'admin@admin.com',
                'email_verified_at' => now(),
                'password' => bcrypt('password'),
                'phone' => '+50300000000',
                'profile_id' => 1,
                'remember_token' => Str::random(10),
                'active' => true,
            ]
        );
    }
}
