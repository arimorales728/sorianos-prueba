<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Database\Seeders\ArtistSeeder;
use Database\Seeders\MusicGenresSeeder;
use Database\Seeders\ProfileSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([ProfileSeeder::class]);
        $this->call([UsersSeeder::class]);
        $this->call([MusicGenresSeeder::class]);
        $this->call([ArtistSeeder::class]);
    }
}
