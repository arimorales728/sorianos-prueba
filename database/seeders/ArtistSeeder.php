<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ArtistSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $json = file_get_contents(database_path('data/artists.json'));
        $artists = json_decode($json, true);
        foreach ($artists as $genre) {
            DB::table('artists')->insert([
                'name' => $genre['name'],
            ]);
        }
    }
}
