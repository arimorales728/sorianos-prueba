<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $json = file_get_contents(database_path('data/profiles.json'));
        $profiles = json_decode($json, true);
        foreach ($profiles as $profile) {
            DB::table('profiles')->insert([
                'name' => $profile['name'],
            ]);
        }
    }
}
