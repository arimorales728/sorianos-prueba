<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('music_genre_user', function (Blueprint $table) {
            $table->foreignId('user_id')->constrained();
            $table->foreignId('music_genre_id')->constrained();
            $table->unique(['user_id', 'music_genre_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('music_genre_user');
    }
};
