import './bootstrap';
import '../css/app.css';

import { createApp, h } from 'vue';
import { createInertiaApp } from '@inertiajs/vue3';
import { resolvePageComponent } from 'laravel-vite-plugin/inertia-helpers';
import { ZiggyVue } from '../../vendor/tightenco/ziggy/dist/vue.m';
import 'maz-ui/styles';
import MazPhoneNumberInput from 'maz-ui/components/MazPhoneNumberInput';
import vueDropzone from 'vue2-dropzone-vue3'
const appName = import.meta.env.VITE_APP_NAME || 'Laravel';

createInertiaApp({
    title: (title) => `${title} - ${appName}`,
    resolve: (name) => resolvePageComponent(`./Pages/${name}.vue`, import.meta.glob('./Pages/**/*.vue')),
    setup({ el, App, props, plugin }) {
        return createApp({ render: () => h(App, props) })
            .use(plugin)
            .use(ZiggyVue)
            .component('MazPhoneNumberInput', MazPhoneNumberInput)
            .component('vueDropzone', vueDropzone)
            .mount(el);
    },
    progress: {
        color: '#4B5563',
    },
});
